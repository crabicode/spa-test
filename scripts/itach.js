
/* eslint-disable no-restricted-syntax, no-console */

const { Socket } = require('net');

process.on('unhandledRejection', (caughtErr) => {
  throw caughtErr;
});

async function main() {
  const COMMAND = 'sendir,1:1,1,37764,1,1,171,170,21,64,21,64,21,64,21,21,21,21,21,21,21,21,21,21,21,64,21,64,21,64,21,21,21,21,21,21,21,21,21,21,21,64,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,64,21,64,21,64,21,64,21,64,21,64,21,64,21,1779,49,292,18,67,14,71,13,72,12,30,8,34,6,36,6,36,5,37,5,80,9,76,10,75,10,32,7,35,5,37,5,37,5,37,5,80,8,34,6,36,6,36,6,36,6,36,6,36,6,36,6,36,5,80,11,74,13,72,14,71,16,69,17,68,17,68,18,1781,89,251,21,64,21,64,21,64,21,21,21,21,21,21,21,21,21,21,21,64,21,64,21,64,21,21,21,21,21,21,21,21,21,21,21,64,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,64,21,64,21,64,21,64,21,64,21,64,21,64,21,3700';

  /**
   * НАСТРОЙКИ
   */
  const host = '192.168.1.107';
  const port = 4998;

  const client = new Socket();

  console.log('[INFO] Connecting %s:%s', host, port);
  client.connect(port, host, () => {
    console.log('[INFO] Connected');
    console.log('[INFO] Sending');
    client.write(COMMAND);
  });

  client.on('data', (data) => {
    console.log('[INFO] Received response: %s', data.toString());
    client.destroy();
  });

  client.on('close', () => {
    console.log('[INFO] Connection closed');
  });
}

if (process.env.NODE_ENV !== 'test') {
  main.call(null);
}
