/* eslint-disable no-await-in-loop, no-restricted-syntax, no-console */

const knx = require('knxnetip');
const { Socket } = require('net');
const { EventEmitter } = require('events');

process.on('unhandledRejection', (caughtErr) => {
  throw caughtErr;
});

const host = '192.168.1.250';
const port = 3671;

const bus = new knx.Connection({ host, port });

const delay = ms =>
  new Promise(resolve => setTimeout(resolve, ms));

class GCConnection extends EventEmitter {
  constructor() {
    super();
    this.socket = null;
  }

  async connect(params) {
    if (this.socket) {
      return undefined;
    }
    return new Promise((resolve, reject) => {
      this.host = params.host;
      this.port = params.port;
      const socket = new Socket();
      socket.once('error', caughtErr => reject(caughtErr));
      socket.once('close', () => { this.socket = null; });
      socket.connect(params.port, params.host, () => resolve());
      this.socket = socket;
    });
  }

  async disconnect() {
    if (this.socket) {
      return new Promise((resolve, reject) => {
        this.socket.once('error', caughtErr => reject(caughtErr));
        this.socket.once('close', () => resolve());
        this.socket.destroy(null);
      });
    }
    return undefined;
  }

  async send(bytes) {
    if (this.socket) {
      return new Promise((resolve) => {
        this.socket.write(bytes, () => resolve());
      });
    }
    throw new Error('Socket not connected');
  }

  static async request(params) {
    const gc = new GCConnection();
    await gc.connect(params);
    const response = new Promise((resolve) => {
      gc.socket.once('data', async (data) => {
        resolve(data.toString());
        await gc.disconnect();
      });
    });
    await gc.send(params.command);
    return response;
  }
}

async function main() {
  // eslint-disable-next-line no-console
  console.log('Connecting to %s:%s', host, port);
  await bus.open();
  // eslint-disable-next-line no-console
  console.log('Connected to %s:%s', host, port);

  /**
   * Сцена
   */

  const scene = [
    {
      address: '15/15/255',
      value: 1,
      encoder: 1.001,
      delay: 150,
    },
    {
      host: '192.168.1.105',
      port: 4998,
      command: 'sendir,1:1,1,39062,1,1,75,12,11,75,11,75,75,12,11,75,11,75,11,75,75,12,11,75,11,75,75,12,75,12,75,12,75,12,11,75,11,75,11,75,11,682\r',
    },
    {
      host: '192.168.1.105',
      port: 4998,
      command: 'sendir,1:1,1,39062,1,1,75,12,11,75,11,75,75,12,11,75,11,75,11,75,75,12,11,75,11,75,75,12,75,12,75,12,75,12,11,75,11,75,11,75,11,682\r',
    },
  ];

  for (const request of scene) {
    if ('encoder' in request) {
      console.log('[INFO] knx sending write to %s', request.address);
      await bus.write(request.encoder, request.value, request.address);
    } else {
      console.log(
        '[INFO] sending to %s:%s %s',
        request.host,
        request.port,
        request.command,
      );
      console.log('[INFO] received %s', await GCConnection.request(request));
    }
    await delay(request.delay);
  }


  process.exit(0);
}

if (process.env.NODE_ENV !== 'test') {
  main.call(this);
}
