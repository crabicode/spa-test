
/* eslint-disable no-restricted-syntax */

const KNX = require('knxnetip');
const GCAdapter = require('gc-adapter');

const knx = new KNX.Connection({
  host: '192.168.1.250',
  port: 3671,
});
const gc = new GCAdapter();

process.on('unhandledRejection', (caughtErr) => {
  throw caughtErr;
});

const delay = ms =>
  new Promise(resolve => setTimeout(resolve, ms));

const scene = [
  {
    host: '192.168.1.149',
    port: 4998,
    output: 1,
    delay: 20,
    command: {
      frequency: 38000,
      irCode: '342,85,21,3661',
      preamble: '342,172,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,63,21,63,21,63,21,63,21,63,21,63,21,21,21,63,21,63,21,21,21,63,21,21,21,63,21,21,21,21,21,21,21,21,21,63,21,21,21,63,21,21,21,63,21,63,21,63,21,1575',
      repeat: 1,
    },
  },
  {
    id: '0/0/58',
    dtp: 1.001,
    value: 1,
    delay: 50,
  },
];

async function main() {
  try {
    console.warn('[INFO] Logic Machine connecting...');
    await knx.open();
    console.warn('[INFO] Logic Machine connected');

    for (const action of scene) {
      if ('command' in action) {
        console.warn(
          '[INFO] GlobalCache sending %s:%s 1:%s',
          action.host,
          action.port,
          action.output,
        );
        await gc.sendir({
          host: action.host,
          port: action.port,
          output: action.output,
          command: action.command,
        });
        console.warn(
          '[OK] GlobalCache sent %s:%s 1:%s',
          action.host,
          action.port,
          action.output,
        );
      } else {
        console.warn('[INFO] KNX write %s to %s', action.value, action.id);
        await knx.write(action.dtp, action.value, action.id);
        console.warn('[OK] KNX sent %s to %s', action.value, action.id);
      }
      await delay(action.wait || 25);
    }
  } catch (caughtErr) {
    console.warn('[ERROR] %s', caughtErr.message);
  } finally {
    try { knx.close(); } catch (err) { /* supress */ }
    try { gc.destroy(); } catch (err) { /* supress */ }
  }
}

if (process.env.NODE_ENV !== 'test') {
  main.call(null);
}
